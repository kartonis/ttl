package timetolive.backend.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {
    public static final String AUTHENTICATION_TAG = "authentication";
    public static final String FOLDERS_TAG = "folders";
    public static final String BLOCK_TAG = "blocks";


    private static final String AUTHENTICATION_DESCRIPTION =
            "Методы, позволяющие пользователю регистрироваться и заходить в приложение";
    private static final String FOLDERS_DESCRIPTION =
            "Методы, позволяющие пользователю работать со вкладками";
    private static final String BLOCK_DESCRIPTION =
            "Методы, позволяющие пользователю работать c блоками";
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("timetolive.backend"))
                .paths(PathSelectors.any())
                .build()
                .tags(new Tag(AUTHENTICATION_TAG, AUTHENTICATION_DESCRIPTION))
                .tags(new Tag(FOLDERS_TAG, FOLDERS_DESCRIPTION))
                .tags(new Tag(BLOCK_TAG, BLOCK_DESCRIPTION));
    }
}
