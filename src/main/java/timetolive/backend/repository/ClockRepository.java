package timetolive.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import timetolive.backend.entity.Clocks;
import timetolive.backend.entity.Folder;

import java.util.Optional;

@Repository
public interface ClockRepository extends CrudRepository<Clocks, Long> {

    Optional<Clocks> findById(Long id);

    Clocks save(Clocks clocks);
    void delete(Clocks clocks);

    Iterable<Clocks> findByFolder(Folder folder);

}
