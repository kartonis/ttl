package timetolive.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import timetolive.backend.entity.Folder;
import timetolive.backend.entity.UserEntity;

import java.util.Optional;

@Repository
public interface FolderRepository extends CrudRepository<Folder, Long> {

    Optional<Folder> findById(Long id);

    Folder save(Folder folder);
    void delete(Folder folder);

    Iterable<Folder> findByUser(UserEntity userEntity);
}