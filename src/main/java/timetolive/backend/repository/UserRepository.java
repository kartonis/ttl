package timetolive.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import timetolive.backend.entity.UserEntity;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> findById(Long id);

    UserEntity save(UserEntity user);
    void delete(UserEntity user);

    Optional<UserEntity> findByEmail(String email);

    Iterable<UserEntity> findByToken(String token);
}