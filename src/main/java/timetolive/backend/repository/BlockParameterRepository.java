package timetolive.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import timetolive.backend.entity.BlockParameter;
import timetolive.backend.entity.Clocks;
import timetolive.backend.entity.Folder;

import java.util.Optional;

@Repository
public interface BlockParameterRepository extends CrudRepository<BlockParameter, Long> {

    Optional<BlockParameter> findById(Long id);

    BlockParameter save(BlockParameter blockParameter);

    BlockParameter findByFolder(Folder folder);

    void delete(BlockParameter blockParameter);
}
