package timetolive.backend.controller.jsonModel.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import timetolive.backend.entity.BlockParameter;
import timetolive.backend.entity.Folder;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FolderInfoResponse {
    private List<BlockInfo> blocks;
    private String token;
}
