package timetolive.backend.controller.jsonModel.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClockRequest {
    String token;
    int x;
    int y;
    int height;
    int width;
    String folderName;
}

