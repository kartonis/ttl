package backend.tests.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import timetolive.backend.entity.BlockParameter;
import timetolive.backend.entity.Clocks;
import timetolive.backend.entity.Folder;
import timetolive.backend.entity.UserEntity;
import timetolive.backend.repository.BlockParameterRepository;
import timetolive.backend.repository.ClockRepository;
import timetolive.backend.repository.FolderRepository;
import timetolive.backend.repository.UserRepository;
import timetolive.backend.service.BlockService;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {FolderRepository.class, BlockService.class, UserRepository.class,  BlockParameterRepository.class})
public class BlockServiceTest {

    @MockBean
    FolderRepository folderRepository;

    @MockBean
    UserRepository userRepository;

    @MockBean
    BlockParameterRepository blockParameterRepository;

    @MockBean
    ClockRepository clockRepository;

    @Autowired
    BlockService blockService;

    @Test
    //@Disabled
    public void getFolderRight() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();
        Clocks clocks = new Clocks(0, 0, 0, 0, folder);

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);
        Mockito.when(clockRepository.save(clocks)).thenReturn(clocks);

        Assertions.assertDoesNotThrow(() -> {
            Clocks clocks1 = blockService.createClock("health", token, 0, 0, 0, 0);
            Assertions.assertEquals(clocks.getFolder().getUser().getToken(), clocks1.getFolder().getUser().getToken());
        });
    }

    @Test
    //@Disabled
    public void getFolderFailure() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();
        Clocks clocks = new Clocks(0, 0, 0, 0, folder);

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);
        Mockito.when(clockRepository.save(clocks)).thenReturn(clocks);

        Assertions.assertDoesNotThrow(() -> {
            blockService.createClock("health", "token123", 0, 0, 0, 0);
            Assertions.assertNotEquals(null, clocks);
        });
    }

    @Test
    //@Disabled
    public void deleteFailure() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();
        Clocks clocks = new Clocks();

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);
        Mockito.when(clockRepository.findById(1L)).thenReturn(Optional.of(clocks));

        Assertions.assertDoesNotThrow(() -> {
            blockService.delete(1L, "token123");
            Assertions.assertFalse(false);
        });
    }

    @Test
    //@Disabled
    public void deleteSuccess() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();
        Clocks clocks = new Clocks();

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);
        Mockito.when(clockRepository.findById(1L)).thenReturn(Optional.of(clocks));

        Assertions.assertDoesNotThrow(() -> {
            blockService.delete(1L, "token");
            Assertions.assertTrue(true);
        });
    }
}

