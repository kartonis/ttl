package backend.tests.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import timetolive.backend.entity.BlockParameter;
import timetolive.backend.entity.Folder;
import timetolive.backend.entity.UserEntity;
import timetolive.backend.repository.BlockParameterRepository;
import timetolive.backend.repository.FolderRepository;
import timetolive.backend.repository.UserRepository;
import timetolive.backend.service.FoldersService;

import java.util.LinkedList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {FolderRepository.class, FoldersService.class, BlockParameter.class, UserRepository.class})
public class FolderServiceTest {

    @MockBean
    FolderRepository folderRepository;

    @MockBean
    UserRepository userRepository;

    @MockBean
    BlockParameterRepository blockParameterRepository;

    @Autowired
    FoldersService folderService;

    @Test
    //@Disabled
    public void createRight() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();

        Mockito.when(folderRepository.save(folder)).thenReturn(folder);
        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);

        Assertions.assertDoesNotThrow(()-> {
            folderService.create("health", "token");
            Assertions.assertTrue(true);
        });
    }

    @Test
    //@Disabled
    public void createWrong() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();

        Mockito.when(folderRepository.save(folder)).thenReturn(folder);
        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);

        Assertions.assertDoesNotThrow(()-> {
            folderService.create("health", "token123");
            Assertions.assertFalse(false);
        });
    }

    @Test
    //@Disabled
    public void getBlocksWrong() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);

        Assertions.assertDoesNotThrow(()-> {
            folderService.getBlocks("token123", "health");
            Assertions.assertNull(null);
        });
    }

    @Test
    //@Disabled
    public void getBlocksRight() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();
        BlockParameter t = new BlockParameter(folder, 0,0,0,0);
        List<BlockParameter> blockParameters = new LinkedList<>();

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);
        Mockito.when(blockParameterRepository.findByFolder(folder)).thenReturn(t);
        blockParameters.add(t);

        Assertions.assertDoesNotThrow(()-> {
            List<BlockParameter> blocks = folderService.getBlocks("token", "health");
            Assertions.assertEquals(blockParameters, blocks);
        });
    }

    @Test
    //@Disabled
    public void deleteRight() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);

        Assertions.assertDoesNotThrow(()-> {
            folderService.delete("health", "token");
            Assertions.assertTrue(true);
        });
    }

    @Test
    //@Disabled
    public void deleteWrong() {
        UserEntity user = new UserEntity("email", "name", "password", "token");
        Folder folder = new Folder("health", user);
        String token = "token";
        List<UserEntity> userEntityList = new LinkedList<>();
        List<Folder> folders = new LinkedList<>();

        Mockito.when(userRepository.findByToken(token)).thenReturn(userEntityList);
        userEntityList.add(user);
        Mockito.when(folderRepository.findByUser(user)).thenReturn(folders);
        folders.add(folder);

        Assertions.assertDoesNotThrow(()-> {
            folderService.delete("health", "token123");
            Assertions.assertFalse(false);
        });
    }

}
